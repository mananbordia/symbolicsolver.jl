# SymbolicSolver

[![Dev](https://img.shields.io/badge/docs-dev-blue.svg)](https://mananbordia.gitlab.io/symbolicsolver.jl/dev)
[![Build Status](https://gitlab.com/mananbordia/SymbolicSolver.jl/badges/main/pipeline.svg)](https://gitlab.com/mananbordia/SymbolicSolver.jl/pipelines)
[![Coverage](https://gitlab.com/mananbordia/SymbolicSolver.jl/badges/main/coverage.svg)](https://gitlab.com/mananbordia/SymbolicSolver.jl/commits/main)
