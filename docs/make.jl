push!(LOAD_PATH,"..")
using SymbolicSolver
using Documenter

DocMeta.setdocmeta!(SymbolicSolver, :DocTestSetup, :(using SymbolicSolver); recursive=true)

makedocs(;
    modules=[SymbolicSolver],
    authors="mananbordia <mananbordia@iitk.ac.in> and contributors",
    repo="https://gitlab.com/mananbordia/SymbolicSolver.jl/blob/{commit}{path}#{line}",
    sitename="SymbolicSolver.jl",
    format=Documenter.HTML(;
        prettyurls=get(ENV, "CI", "false") == "true",
        canonical="https://mananbordia.gitlab.io/SymbolicSolver.jl",
        edit_link="main",
        assets=String[],
    ),
    pages=[
        "Home" => "index.md",
        "Tools" => "tools.md",
        "Comparison Against CASs" => "compare.md"
    ],
)

deploydocs(
    repo = "gitlab.com/mananbordia/SymbolicSolver.jl.git",
)
