```@meta
CurrentModule = SymbolicSolver
```

# Comparison Against Computer Algebra Systems

We compare our software vs. solving routines in Maple, Sympy, and Reduce.

### Interface

- Maple


```
>> solve({x^2 - 2x + 1 = 0}, [x]);

Out : [[x = 1], [x = 1]]
```

- SymbolicSolver


```
>> solve_system([x], [x^2 - 2x + 1])

Out : [[1//1]]
```

- Python (Sympy)

```
>> solve((x**2 - 2*x + 1), [x])

Out : [1]
```

- Reduce

```
>> solve({x^2 - 2x + 1}, x);

Out : {x=1}
```

### Solving Polynomials

|     | Maple  | Sympy | Reduce |
| --- | -----  | ----- | -----  |
| x - 1 | `[[x = 1]]` | `[1]` | `{x=1}` |
| x^2 - 2x + 1 | `[[x = 1], [x = 1]]` | `[1]` | `{x=1}` |
| x^2 - 2 | **[a]** | `[-sqrt(2), sqrt(2)]` | `{x=sqrt(2),x= - sqrt(2)}`|
| x^2 + 1 | `[[x = I], [x = -I]]` | `[-I, I]` | `{x=i,x= - i}` |
| x^4 - 1 | `[[x = 1],[x = -1],[x = I],[x = -I]]` | `[-1, 1, -I, I]` | `{x=i,x= - i,x=1,x=-1}` |
| (x - 3)(x + 2)(x - 8)(x - 21) | `[[x = -2],[x = 3],[x = 8],[x = 21]]` | `[-2, 3, 8, 21]` | `{x=21,x=8,x=3,x=-2}` |

- **[a]**

```
           1/2          1/2
    [[x = 2   ], [x = -2   ]]
```

#### (x^2 + 1)*(x - 5/4)^2*(x^3 - 1)

- Reduce

```
solve({(x^2 + 1)*(x - 5/4)^2*(x^3 - 1)});

Out :
      5       sqrt(3)*i - 1
{ x= ---, x= ---------------,
      4             2

      -(sqrt(3)*i + 1)
  x= -----------------, x=i,
             2

  x=i, x= - i, x=1 }
```

- Maple

```
solve({(x^2 + 1)*(x - 5/4)^2*(x^3 - 1)}, [x]);

[[x = 5/4], [x = 5/4], [x = I],
[x = -I], [x = 1],
                    1/2
[x = - 1/2 - 1/2 I 3   ],
                    1/2
[x = - 1/2 + 1/2 I 3   ]]
```

#### ax^2 + bx + c

- Reduce

```
solve({a*x^2 + b*x + c}, x);

Out :
                     2
    sqrt( - 4*a*c + b ) - b
{x=-------------------------,
              2*a

                         2
     - (sqrt( - 4*a*c + b ) + b)
 x=------------------------------}
                2*a
```

- Maple

```
solve({a*x^2 + b*x + c}, [x]);

Out :
                        2 1/2
        -b + ( - 4ac + b )
[[x =  ----------------------],
                2a

                        2 1/2
         -b + ( - 4ac + b )
 [x = - ----------------------]]
                   2a
```

#### x^8 - 1

- Reduce

```
solve({x^8 - 1}, x);

Out :

{ x=sqrt( - i), x= - sqrt( - i),
 x=sqrt(i), x= - sqrt(i),
 x=i, x= - i,
 x=1, x=-1 }
```

### Solving Systems

|| Maple  | something |
|-| ------------- | ------------- |

### Disclaimer

We are using

- Maple v2021
- Python v3.9.5 (sympy version 1.10.1)
- Reduce svn revision 6346
