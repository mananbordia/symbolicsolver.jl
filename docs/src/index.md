```@meta
CurrentModule = SymbolicSolver
```

# SymbolicSolver

Check out source code for [SymbolicSolver](https://gitlab.com/mananbordia/SymbolicSolver.jl).



### Installing Package

You can obtain SymbolicsSolver using Julia's Pkg REPL-mode (hitting `]` as the first character of the command prompt):

```julia
(@v1.6) pkg> add "https://gitlab.com/mananbordia/symbolicsolver.jl.git"
```

or with `using Pkg; Pkg.add(url = "https://gitlab.com/mananbordia/symbolicsolver.jl.git")`


You can also check the version of package installed:

```julia
(@v1.6) pkg> status SymbolicSolver
# Output
Status `~/.julia/environments/v1.6/Project.toml`
  [86411fd0] SymbolicSolver v0.1.1 `https://gitlab.com/mananbordia/symbolicsolver.jl#main`
```

### Importing Package Tools

```julia
julia> using SymbolicSolver

```
That's all you need to do. Now you are ready to use the tools provided by package.


### What problem are we solving ?
SymbolicSolver aims to solve Multivariate Polynomial Systems Symbolically.

### Our Current Approach :
```
1) Get Groebner Basis for the given system of polynomial.
2) The first equation in the Groebner Basis will be univariate polynomial. Find the roots of the first equation.
  i) Roots of univariate polynomial equation are found using factorisation.
3) One by one substitute these roots into remaining polynomial equation.
  i) By the nature of Groebner Basis the next (2nd) polynomial will become univariate polynomial.
4) Apply from step 2 and 3 again on the remaining Polynomial System (2nd onwards) recursively.
5) Merge the solution from Univariate Polynomial and Multivariate System of Polynomial to get the complete solution.
```

### Current Scope of the package:
* The solver can find all the roots of the system of polynomial which are Rational and all other roots are neglected.
* System with infinite roots are unsolvable for now.

### What Future Goals we want to achieve ?
As we go along the time we have following plans for this package and this is how it is going to expand this package :
- [x] Implement solver for Polynomial System to find only Rational Roots.
- [ ] Expand solver to support both Rational and Irrational Roots (Real roots).
- [ ] Expand solver to support both Real and Imaginary Roots.
- [ ] Expand solver to support Real, Imaginary and Parametric Roots.

### Packages used :
* [AbstractAlgebra.jl](https://nemocas.github.io/AbstractAlgebra.jl/dev/)
* [Groebner.jl](https://sumiya11.github.io/Groebner.jl)
* [Symbolics.jl](https://symbolics.juliasymbolics.org/dev/)
* [PolynomialFactors.jl](https://github.com/jverzani/PolynomialFactors.jl)
