# Add some checks and that should be fine
sym_to_polyvars(Sym::Vector{Num}) = PolynomialRing(QQ,[string(sym.val.name) for sym in Sym])
sym_to_polyvars(Sym::Vector{Symbol}) = PolynomialRing(QQ,[string(sym) for sym in Sym])
# Now,probably don't even need Vars
sympoly_to_poly(Vars, Sym) = substitute(Sym, Dict(zip(Vars, sym_to_polyvars(Vars)[2])))
