function gcd(a::BigInt, b::BigInt)
    if a == 0
        return b
    else
        return gcd(b%a, a)
    end
end

lcm(a::BigInt,b::BigInt) = (a*b) ÷ gcd(a,b)

function lcm(dens::Vector{BigInt})
    if length(dens) == 1
        return only(dens)
    else
        mid = length(dens) ÷ 2
        lcm(lcm(dens[1:mid]), lcm(dens[mid+1:end]))
    end
end

function normalise_dens!(x::AbstractAlgebra.Generic.Poly{Rational{BigInt}})
    # we are just multiplying the polynomial with
    # smallest factor such that all coeffs. have 1 as denominator
    # We are performing this because poly_factor shows unexpected
    # results for fractional coeffs. Check - x^2 + x + 1//4

    normalisingFactor = lcm(map((q)-> q.den, x.coeffs))
    x.coeffs *= normalisingFactor
    return
end

"""
m_poly_factor(y)

Normailses the polynomial and returns its factors.

    INPUT :
        Compulsory arguments
            poly - Polynomial of type AbstractAlgebra.Generic.Poly{T}

    OUTPUT :
        Returns Dictionary of factors where key is the factor and value is power of the factor.

# Example
```julia-repl

julia> using SymbolicSolver

julia> using AbstractAlgebra

julia> _, x = PolynomialRing(QQ, 'x')
(Univariate Polynomial Ring in x over Rationals, x)

julia> poly = x^2 + 3x + 2
x^2 + 3*x + 2

julia> m_poly_factor(poly)
Dict{AbstractAlgebra.Generic.Poly{Rational{BigInt}}, Int64} with 2 entries:
  x + 1 => 1
  x + 2 => 1

```
"""
function m_poly_factor(poly)
    normalise_dens!(poly)
    return poly_factor(poly)
end
