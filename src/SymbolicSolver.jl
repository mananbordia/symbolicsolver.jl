module SymbolicSolver
    export m_poly_factor, solve_system
    using AbstractAlgebra, PolynomialFactors, Symbolics, Groebner
    include("ModifiedPolyFactor.jl")
    include("Converters.jl")
    include("SystemPolySolver.jl")
    include("SystemSymbolicSolver.jl")
    include("UniPolySolver.jl")
    # Write your package code here.
end
