"""
solve_system(Vars::Vector{Num}, Sys::Vector{Num}, debug::Int64=0)

Find solution for the given Symbolic Polynomial System.

    INPUT :
        Compulsory arguments
            Vars - Vector of Symbolic Variables
            Sys - Vector of Symbolic System of Polynomial Equations

        Optional
            debug - Set 1 to print grobner basis and order of variables. Default = 0

    OUTPUT :
        Returns Vector of all Possible Rational Solutions.

# Example
```julia-repl

julia> using SymbolicSolver

julia> using Symbolics

julia> @variables x y
2-element Vector{Num}:
 x
 y

julia> PolySys = [x^2 - 9, x^2 - y^2]
2-element Vector{Num}:
     x^2 - 9
 x^2 - (y^2)

julia> solve_system([x, y], PolySys, debug=1)
[ Info: Order of solution : ["x", "y"]
groebner(Polysys) = AbstractAlgebra.Generic.MPoly{Rational{BigInt}}[y^2 - 9, x^2 - 9]
4-element Vector{Vector{Rational{BigInt}}}:
 [-3//1, -3//1]
 [3//1, -3//1]
 [-3//1, 3//1]
 [3//1, 3//1]

julia> @variables x y z
3-element Vector{Num}:
x
y
z

julia> SymVars = [x, y, z]
3-element Vector{Num}:
x
y
z

julia> SymPoly = [z^2 - x, x - 1,  y + z - x]
3-element Vector{Num}:
   z^2 - x
     x - 1
 y + z - x

julia> solve_system(SymVars, SymPoly)
2-element Vector{Vector{Rational{BigInt}}}:
[1//1, 2//1, -1//1]
[1//1, 0//1, 1//1]

```
"""
function solve_system(SymVars::Vector{Num}, SymSys::Vector{Num}, debug::Int64=0)
    Polyvars = sym_to_polyvars(SymVars)[2]
    Polysys = [sympoly_to_poly(SymVars,poly) for poly in SymSys]

    if debug == 1
        @info string("Order of solution : ", [string(var) for var in Polyvars[1].parent.S])
        @show groebner(Polysys)
    end
    find_system_roots(Polyvars, groebner(Polysys))
end
