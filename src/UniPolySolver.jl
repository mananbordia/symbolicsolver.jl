function solve_linear(x::AbstractAlgebra.Generic.Poly{Rational{BigInt}})
    if x.length == 2
        return - x.coeffs[1] // x.coeffs[2]
    end
end

# Finds only rational roots
function find_roots(poly::AbstractAlgebra.Generic.Poly{Rational{BigInt}})
    polyfactors = m_poly_factor(poly)
    roots = Vector()
    for key in keys(polyfactors)
        root = solve_linear(key)
        if root != nothing
            push!(roots, root)
        end
    end
    return roots
end
